import configparser
from pymongo import MongoClient, errors, ReturnDocument
# import random
# import string
# import time
import requests
from bson.objectid import ObjectId
import ssl

class Utils:
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('.env')

        # connectionString = self.config['mongo_db_test']['string']
        connectionString = self.config['mongo_db_live']['string']
        # ssl_cert_path    = self.config['mongo_db_test']['ssl_cert_path']
        # ssl_ca_cert_path = self.config['mongo_db_test']['ssl_ca_cert_path']
        
        try:
            conn = MongoClient(connectionString,
                                # ssl=True,
                                # ssl_certfile=ssl_cert_path,
                                # ssl_cert_reqs=ssl.CERT_REQUIRED,
                                # ssl_ca_certs=ssl_ca_cert_path
                                )
            print("connected to db......")

        except Exception as e:
            print("Not connected to db....", e)

        self.employee       = conn.olvo_store['employees']
        self.route          = conn.olvo_store['routes']
        self.shop           = conn.olvo_store['shops']
        self.topup_purchase = conn.olvo_store['topup_purchases']
        
    def validateObjectId(self,Object_id_list):
        try:
            for object_id in Object_id_list:
                if ObjectId.is_valid(object_id):
                    pass 
                else:
                    return False,f"{object_id} is not valid object id",400
            return True,"OK",200
        except Exception as e:
            print("error in object id validation",e)
            return False,str(e),500

    def switchSalesman(self,oldId,newId,shop):
        try:
            print(oldId,newId,shop)

            old_salesman = list(self.employee.find({"_id":ObjectId(oldId)}))
        
            if len(old_salesman) == 0:
                return False,"No such Old salesman found",404

            new_salesman = list(self.employee.find({"_id":ObjectId(newId)}))
           
            if len(new_salesman) == 0:
                return False,"No such new salesman found",404
            
            # shops = list(self.shop.find({"_id":ObjectId(shop)}))
            
            # if len(shops) == 0:
            #     return False,"No such shop found",404

            if ObjectId(shop) not in old_salesman[0]['shop_id']:
                return False,"No such shop found for the current salesman",404
            
            try:
                switchingRoute = (self.route.find_one_and_update(
                    { 
                        "employee_id" : ObjectId(newId),
                        "name"        : "Others"
                        },
                        
                    {'$set':
                    {
                        "name"        : "Others",
                        "description" : "Others",      
                        "employee_id" : ObjectId(newId),
                        "isDeleted"   : False,
                        "status"      : 1
                    }},
                    upsert = True
                    ))
            except Exception as e:
                print("error in changing route : ",e)
                return False,str(e),500
            
            route = list(self.route.find(
                    { 
                        "employee_id" : ObjectId(newId)}))
            route_id = route[0]['_id']
            
            try:
                result = self.employee.update_one(
                { "_id": ObjectId(oldId) },
                { "$pull": { "shop_id": ObjectId(shop) } }
                )     
                print(result)
                if result.modified_count == 0:
                    return False,"error in removing shops from old employee",500
            except Exception as e:
                print("error in removing shops from old employee : ",e)
                return False,str(e),500
            
            try:
                result = self.employee.update_one(
                { "_id": ObjectId(newId) },
                { "$push": { "shop_id": ObjectId(shop) } } 
                )
                print(result.modified_count)
                if result.modified_count == 0:
                    return False,"error in adding shops into new employee",500
            except Exception as e:
                print("error in adding shops into new employee : ",e)
                return False,str(e),500

            try:
                result = (self.shop.update_one(
                { "_id" : ObjectId(shop)},
                {"$set" : { "route_id": ObjectId(route_id)}}

                ))
                print(result)
                if result.modified_count == 0:
                    return False,"error in changing routes of shop",500
            except Exception as e:
                print("error in changing routes of shops: ",e)
                return False,str(e),500
            try:
                
                result = (self.topup_purchase.update_many(
                { "shop_id" : ObjectId(shop)},
                {"$set" : { "employee_id" : ObjectId(newId),
                            "topup_purchase_employee_id" : ObjectId(oldId)}}
                ))
                print(result)
            except Exception as e:
                print("error in changing employee idin topup recharge: ",e)
                return False,str(e),500
            
            return True,"Success",200

        except Exception as e:
            print(e)
            return False,str(e),500
        
# ob = Utils()
# ob.changeObjectId("5fc0847c01d06c25f5535870")