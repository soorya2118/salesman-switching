from flask import Flask,jsonify,request
from utils import Utils


app = Flask(__name__)

@app.route('/')
def hello_world():
    s= "Hello"
    return s

@app.route('/switch/employee/',methods=['POST'])
def switchSalesman():
    try:
        old_employee_id = request.form.get("old_employee_id")
        new_employee_id = request.form.get("new_employee_id")
        shop_id         = request.form.get("shop_id")

        if (not old_employee_id) or (not new_employee_id) or (not shop_id):
            response = {
                "code"    : 400,
                "message" : "Bad input",
                "status"  : False
            }
            return jsonify(response),400
        
        status,response,code = utils.validateObjectId([old_employee_id,new_employee_id,shop_id])
        # print(status,response,code)
        if status == False:
            response = {
                "code"    : code,
                "message" : response,
                "status"  : False
            }
            return jsonify(response),code
        
        status,response,code = utils.switchSalesman(old_employee_id,
                                                    new_employee_id,shop_id)
        print(status,response,code)
        if status == False:
            response = {
                "code"    : code,
                "message" : response,
                "status"  : False
            }
            return jsonify(response),code
        response = {
                "code"    : 200,
                "message" : "Sucess !!!",
                "status"  : True
            }
        return jsonify(response),200



    except Exception as e:
        print(e)
        response = {
            "code"    : 500,
            "message" : str(e),
            "status"  : False
        }
        return jsonify(response),500

if __name__ == '__main__' :

    utils = Utils()

    app.run(debug=True,host='0.0.0.0', port='5000')